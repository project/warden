<?php

namespace Drupal\warden\Commands;

use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class WardenCommands extends DrushCommands {

  /**
   * Test the connection to Warden by getting its public key
   *
   *
   * @command warden:check
   * @aliases warden-check
   */
  public function check() {
    try {
      $warden = \Drupal::service('warden.manager');
      drush_print(dt('URL: :url', [':url' => $warden->getWardenUrl()]));

      if ($warden->hasBasicAuthentication()) {
        drush_print(dt('HTTP Username: :username', [':username' => $warden->getUsername()]));
        drush_print(dt('HTTP Password: :password', [':password' => $warden->getPassword()]));
      }

      if ($warden->hasCertificatePath()) {
        drush_print(dt('Certificate file: :path', [':path' => $warden->getCertificatePath()]));
      }

      $key = $warden->getPublicKey();

      drush_print(dt('Going to check connection to Warden server by retrieving the public key ...'));
      drush_print($key);
    }
    catch (Exception $e) {
      drush_set_error($e->getMessage());
    }
  }

  /**
   * Update Warden with the lastest site data
   *
   *
   * @command warden:update
   * @aliases warden-update
   */
  public function update() {
    drush_print(dt('Going to update Warden ...'));

    try {
      $warden_manager = \Drupal::service('warden.manager');
      $warden_manager->updateWarden();
      drush_print(dt('... success'));
    }
    catch (Exception $e) {
      return drush_set_error($e->getMessage());
    }
  }

  /**
   * Displays the module data that will be sent to Warden
   *
   *
   * @command warden:show-module-data
   * @aliases warden-show-module-data
   */
  public function showModuleData() {
    module_load_include('inc', 'warden', 'warden.page');
    $warden_manager = \Drupal::service('warden.manager');
    $data = $warden_manager->generateSiteData();
    $this->output()->writeln($data);
  }

}
